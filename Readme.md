# Mockete

As simple as a mocked server can get.

If you want `/api/v1/users` to return `[{"user_id": 1}, {"user_id": 2}]`, then
create a file structure like `api/v1/user.json` with the `user.json` file
containing the desired response.

**Mockete** will reflect your file structure as a rest api!

To run it as a docker container:

```
docker run -p "3000:3000" -v /path/to/my/mocks/folder:/app/mocks uesteibar/mockete:latest
```

## Roadmap

- [ ] Add support for parameters (via erb files)
- [ ] Add suport for other response formats

