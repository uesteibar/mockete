FROM ruby:2.5-alpine

WORKDIR /app
ADD . /app
RUN bundle install

EXPOSE 3000
CMD ["ruby", "/app/main.rb"]
