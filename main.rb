require 'sinatra'

set :bind, '0.0.0.0'
set :port, 3000

get '*' do
  begin
    File.read("./mocks#{request.path_info}.json")
  rescue => _error
    halt 404
  end
end

not_found do
  erb :'404', locals: { path: request.path }, format: :html
end
